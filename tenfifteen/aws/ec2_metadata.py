import urllib2
import boto.ec2
import re

ROUTE53_RECORD_TYPE = 'CNAME'

EC2_METADATA_URL = 'http://169.254.169.254/latest/meta-data'
EC2_METADATA_RESOURCE_PUBLIC_HOSTNAME = 'public-hostname'
EC2_METADATA_RESOURCE_INSTANCE_ID= 'instance-id'
EC2_METADATA_RESOURCE_AVAILABILITY_ZONE = 'placement/availability-zone'
EC2_METADATA_TAG_NAME= 'Name'

###############################################################################
# EC2 Instance metadata functions
###############################################################################
class EC2ConnectionError(Exception):
    """ Exception raised for ec2 boto connection errors """
    def __init__(self, region):
        self.region=region


class EC2MetadataError(Exception):
    """ Exception raised for ec2 instance metadata query errors """
    def __init__(self, url):
        self.url=url

class EC2TagError(Exception):
    """ Exception raised for ec2 instance tag query errors """
    def __init__(self, tag):
        self.tag=tag 


def ec2_instance_public_hostname_url():
    """ Return the URL to query the ec2 instance metadata public hostname """
    return "{0}/{1}".format(EC2_METADATA_URL, EC2_METADATA_RESOURCE_PUBLIC_HOSTNAME)


def ec2_instance_id_url():
    """ Return the URL to query the ec2 instance metadata public hostname """
    return "{0}/{1}".format(EC2_METADATA_URL, EC2_METADATA_RESOURCE_INSTANCE_ID)


def ec2_instance_availability_zone_url():
    """ Return the URL to query the ec2 instance metadata public hostname """
    return "{0}/{1}".format(EC2_METADATA_URL, EC2_METADATA_RESOURCE_AVAILABILITY_ZONE)


def get_ec2_instance_metadata(url):
    """ Return a metadata item for a given amazon ec2 metadata url """
    try:
        req = urllib2.Request(url)
        response = urllib2.urlopen(req)
        return response.read()
    except urllib2.URLError:
        raise EC2MetadataError(url)


def ec2_instance_id():
    """ Return the EC2 instance id from the ec2 instance metadata """
    return get_ec2_instance_metadata(ec2_instance_id_url())


def ec2_instance_public_hostname():
    """ Return the EC2 instance id from the ec2 instance metadata """
    return get_ec2_instance_metadata(ec2_instance_public_hostname_url())


def ec2_instance_availability_zone():
    """ Return the EC2 instance availability zone from the ec2 instance metadata """
    return get_ec2_instance_metadata(ec2_instance_availability_zone_url())


def ec2_instance_region():
    """ Return the EC2 instance region from the ec2 instance metadata """
    return re.findall(r"^[a-z]+-[a-z]+-[0-9]+", ec2_instance_availability_zone())[0]


def ec2_boto_instances(connection):
    """ Return a list of all instances for a given boto connection """
    reservations = connection.get_all_instances()
    return [i for r in reservations for i in r.instances]


def ec2_boto_instance_tags(connection, instance_id):
    """ Return a dictionary of instnace tags given a boto connection and and the instance-id """
    for instance in ec2_boto_instances(connection):
        if instance.__dict__['id'] == instance_id:
            return instance.__dict__['tags']


def ec2_boto_region_connection():
    """ Return a boto EC2 connection for the current region """
    region = ec2_instance_region()
    connection = boto.ec2.connect_to_region(region)
    if not connection:
        raise EC2ConnectionError(region)
    return connection


def ec2_tag_name(connection):
    """ Return the instance name tag value """
    instance_tags = ec2_boto_instance_tags(connection, ec2_instance_id())
    try:
        return instance_tags['Name']
    except KeyError:
        raise EC2TagError('Name')

