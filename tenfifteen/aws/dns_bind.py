""" Ensure a DNS CNAME in AWS Route53 points to amazon's instance fqdn
   A CNAME pointing to the amazon instance fqdn is used because the 
   amazon fqdn resoves to a private IP address internally and the public
   IP address externally to save on uncessary traffic costs VM to VM """
    

import sys
import logging
import argparse
from tenfifteen.aws.ec2_metadata import *
from tenfifteen.aws.route53 import fqdn, route53_zone, address_records, Route53ZoneError

DRY_RUN = False

# global logging object
logger = logging.getLogger('DNS-BIND')

###############################################################################
# Command line options
###############################################################################
def log_levels():
    return { 
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warn': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL,
    }


def cmd_line_args():
    parser = argparse.ArgumentParser(description='Dynamically create or update a Route53 CNAME record using the current instance Name tag.')
    parser.add_argument('zone_name', metavar='ZONE_NAME', help='DNS zone name to register host in')
    parser.add_argument('--log-level', dest='log_level', metavar='LOG_LEVEL', choices=log_levels().keys(), default='warn', help='Logging verbosity')
    return parser.parse_args()


def set_logger_to_console(logger):
    ch = logging.StreamHandler()
    logger.addHandler(ch)


def set_logger_verbosity(args):
    logger.setLevel(log_levels()[args.log_level])


###############################################################################
# Main
###############################################################################
def main():
    args = cmd_line_args()
    set_logger_to_console(logger)
    set_logger_verbosity(args)

    try:
        # Log some info on startup
        logger.info("Instance ID '{0}'".format(ec2_instance_id()))
        logger.info("Instance region '{0}'".format(ec2_instance_region()))

        # get the instance tag to use as a hostname
        ec2conn = ec2_boto_region_connection()
        hostname = ec2_tag_name(ec2conn)
        logger.info("Instance name tag '{0}'".format(hostname))

        # get Route53 zone
        zone = route53_zone(args.zone_name)
        zone_records = zone.get_records()

        # make the hostname fully qualified
        fq_hostname = fqdn(hostname, args.zone_name).lower()
        logger.info("Instance fqdni '{0}'".format(fq_hostname))

        # Determine if we are updating or adding
        if fq_hostname in [r.name.lower() for r in address_records(zone_records)]:
            # update
            logger.info("Updating existing dns record".format(fq_hostname))
            not DRY_RUN and zone.update_cname(fq_hostname, ec2_instance_public_hostname())
        else:
            # add
            logger.info("Adding new dns record".format(fq_hostname))
            not DRY_RUN and zone.add_cname(fq_hostname, ec2_instance_public_hostname())
            
    except boto.exception.NoAuthHandlerFound:
        logger.critical("Unable to find AWS credentials.")
        sys.exit(2)
    except EC2MetadataError as detail:
        logger.critical("Unable to connect to amazon's ec2 instance metadata services url '{0}'".format(detail.url))
        sys.exit(2)
    except EC2ConnectionError as detail:
        logger.critical("Unable to connect to amazon's ec2 boto services in region '{0}'".format(detail.region))
        sys.exit(2)
    except Route53ZoneError as detail:
        logger.critical("No zone found for zone name {0}".format(detail.zone_name))
        sys.exit(2)
    except EC2TagError as detail:
        logger.critical("Unable to query instance tag '{0}'".format(detail.tag))
        sys.exit(2)


if __name__== '__main__':
    main()
