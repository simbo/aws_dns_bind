import area53 

ROUTE53_RECORD_TYPE = 'CNAME'
DRY_RUN = False

###############################################################################
# Route53 functions
###############################################################################
class Route53ZoneError(Exception):
    """ Exception raised for route 53 errors """
    def __init__(self, zone_name):
        self.zone_name=zone_name


def fqdn(name, zone):
    """ Return a fully qualified doman name given a hostname and zonename """
    return area53.route53.make_qualified(name + '.' + zone)


def address_records(zone_records):
    """ Return a list of cname records given a full set of zone records """
    return [ i for i in zone_records if i.type == ROUTE53_RECORD_TYPE ]


def route53_zone(zone_name):
    """ Return the route53 zone record for a given zone name """
    zone = area53.route53.get_zone(zone_name)
    if not zone:
        raise Route53ZoneError(zone_name)
    return zone

