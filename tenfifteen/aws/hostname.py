import sys
import logging
import argparse
from subprocess import check_call, CalledProcessError
from tenfifteen.aws.ec2_metadata import *
from tenfifteen.aws.route53 import fqdn

HOSTNAME_PATH = "/bin/hostname"
DRY_RUN = False

# global logging object
logger = logging.getLogger('AWS-HOSTNAME')


###############################################################################
# Hostname
###############################################################################
class HostnameError(Exception):
    def __init__(self, output):
        self.output = output


def set_hostname(hostname):
    try:
        check_call([HOSTNAME_PATH, hostname])
    except CalledProcessError:
        raise HostnameError()


###############################################################################
# Command line options
###############################################################################
def log_levels():
    return { 
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warn': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL,
    }


def cmd_line_args():
    parser = argparse.ArgumentParser(description='Dynamically create or update a Route53 CNAME record using the current instance Name tag.')
    parser.add_argument('zone_name', metavar='ZONE_NAME', help='DNS zone name')
    parser.add_argument('--log-level', dest='log_level', metavar='LOG_LEVEL', choices=log_levels().keys(), default='warn', help='Logging verbosity')
    return parser.parse_args()


def set_logger_to_console(logger):
    ch = logging.StreamHandler()
    logger.addHandler(ch)


def set_logger_verbosity(args):
    logger.setLevel(log_levels()[args.log_level])


###############################################################################
# Main
###############################################################################
def main():
    args = cmd_line_args()
    set_logger_to_console(logger)
    set_logger_verbosity(args)

    try:
        # Log some info on startup
        logger.info("Instance ID '{0}'".format(ec2_instance_id()))
        logger.info("Instance region '{0}'".format(ec2_instance_region()))

        # get the instance tag to use as a hostname
        ec2conn = ec2_boto_region_connection()
        hostname = ec2_tag_name(ec2conn)
        logger.info("Instance name tag '{0}'".format(hostname))

        # make the hostname fully qualified
        fq_hostname = fqdn(hostname, args.zone_name).lower()
        logger.info("Instance fqdni '{0}'".format(fq_hostname))

        # set the hostname
        set_hostname(fq_hostname)
            
    except boto.exception.NoAuthHandlerFound:
        logger.critical("Unable to find AWS credentials.")
        sys.exit(2)
    except EC2MetadataError as detail:
        logger.critical("Unable to connect to amazon's ec2 instance metadata services url '{0}'".format(detail.url))
        sys.exit(2)
    except EC2ConnectionError as detail:
        logger.critical("Unable to connect to amazon's ec2 boto services in region '{0}'".format(detail.region))
        sys.exit(2)
    except EC2TagError as detail:
        logger.critical("Unable to query instance tag '{0}'".format(detail.tag))
        sys.exit(2)
    except HostnameError as detail:
        logger.critical("Unable to set hostname")
        sys.exit(2)


if __name__== '__main__':
    main()
