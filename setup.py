from setuptools import setup, find_packages

################################################################################
# Setup
################################################################################
setup(
    name="aws_dns_bind",
    version="0.0.1",
    description="A script to dynamically create a Route53 CNAME record using the instance Name tag.",
    author="Simon Inglis",
    packages=find_packages(exclude="test"),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "argparse",
        "Area53",
        "boto",
    ],
    entry_points={
        'console_scripts': [
            'aws_dns_bind = tenfifteen.aws.dns_bind:main',
            'aws_hostname = tenfifteen.aws.hostname:main',
        ]
    },
)
